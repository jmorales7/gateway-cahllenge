import { IDataAccess } from 'infrastructure/data-access/IDataAccess';
import { IServices } from 'infrastructure/services/IServices';

import { ILogger } from '../../../../logger/Logger';
import { CustomError, Errors } from '../../../entities/shared/Errors';

export type InitConfig = {};

export const initialSetup =
  (services: IServices, dataAccess: IDataAccess, logger: ILogger) => async (): Promise<any> => {
    try {
      services.orderBookService.getOrderBook({ name: 'tBTCUSD' });
    } catch (error: any) {
      throw new CustomError(Errors.SERVER_ERROR, error);
    }
  };

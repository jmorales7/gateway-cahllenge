import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

export const market =
  ({ orderBookService }: IServices, logger: ILogger) =>
  async ({ pairName, operationType, amount }: any): Promise<any> => {
    console.log(amount, 123456);
    return await orderBookService.market({ pairName, operationType, amount });
  };

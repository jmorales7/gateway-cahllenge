import { IServices } from 'infrastructure/services/IServices';
import { ILogger } from 'logger/Logger';

import { GetOrderBookDTO } from '../../../entities/OrderBook/OrderBook.dto';

export const getTips =
  ({ orderBookService }: IServices, logger: ILogger) =>
  async ({ name }: GetOrderBookDTO): Promise<any> => {
    return await orderBookService.get({ name });
  };

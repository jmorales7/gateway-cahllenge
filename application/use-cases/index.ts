import { IServices } from 'infrastructure/services/IServices';

import { ILogger } from '../../logger/Logger';
import { getTips } from './OrderBook/GetTips/GetTips';
import { market } from './OrderBook/Market/Market';
import { getOrderBook } from './OrderBook/get/Get';

export const useCaseFactory = (services: IServices, baseLogger: ILogger) => ({
  getOrderBook: getOrderBook(services, baseLogger.child({ controller: 'getOrderBook' })),
  getTips: getTips(services, baseLogger.child({ controller: 'getTips' })),
  market: market(services, baseLogger.child({ controller: 'market' }))
});

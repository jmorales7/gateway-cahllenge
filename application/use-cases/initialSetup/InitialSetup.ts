import { InitialSetupError } from './Errors';

export type InitialSetupDependencies = {
  // TODO: add dependencies
};

export const initialSetup =
  (initialSetup: InitialSetupDependencies) =>
  async (params: any): Promise<string> => {
    try {
      // TODO: add logic
      return 'Application initialized!';
    } catch (error) {
      throw new InitialSetupError(error);
    }
  };

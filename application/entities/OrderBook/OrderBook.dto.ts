import { z } from 'zod';

export const GetOrderBookDTO = z.object({
  name: z.string()
});

export type GetOrderBookDTO = z.infer<typeof GetOrderBookDTO>;

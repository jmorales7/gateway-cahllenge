import { GetOrderBookDTO } from './OrderBook.dto';

export type IOrderBook = {
  getOrderBook(params: GetOrderBookDTO): Promise<any>;
  get(params: any): Promise<any>;
  market(params: any): Promise<any>;
};

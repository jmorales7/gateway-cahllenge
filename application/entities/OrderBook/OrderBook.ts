export interface OrderBook {
  bids: Order[];
  asks: Order[];
}

export interface Order {
  price: number;
  count: number;
  amount: number;
}

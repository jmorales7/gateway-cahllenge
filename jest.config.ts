export default {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}', // Extensiones de archivos que se deben incluir en la cobertura
    '!**/node_modules/**', // Directorios que se deben excluir de la cobertura
    '!**/coverage/**', // Directorio donde se generan los informes de cobertura
    '!**/dist/**'
  ],
  coverageReporters: ['lcov', 'text']
};

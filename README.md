####

# Challenge - TS & Clean Architecture boilerplate

## Description

Clean architecture offers benefits such as modularity, technology independence, testability, maintainability, code reusability, and scalability. It helps create more robust, flexible, and easily maintainable systems.

## Folders

```
application: contains the business logic
infraestructure: contains all the technologies and logic of the project
entities: definition of entities and interfaces that will be applied
use-cases: contains all the use cases your project needs
data-access: all external services that our project will implement as external apis or databases
services: the logic of our project
interfcaes: configuration of our server who is the one who receives and presents the information, middlewares, routes etc are configured
```

## Implementation

```
The project is implemented with a clean architecture template that is
fully prepared to build an API with Husky,Docker, Express.js, TypeScript, and MongoDB.
The project incorporates different technologies to maintain good practices and standardized
code, relieving us from worrying about indentation, single or double quotes.
The project's abstraction is designed to receive a CSV with columns for balance and description,
and for each of our cryptocurrencies, the annual balance will be calculated and the record will
be saved in MongoDB. This will allow us to later retrieve the information in a paginated manner or by ID.
```

#### Run :

- install dependencies:

```
yarn install
```

- Run project:

```
yarn dev
```

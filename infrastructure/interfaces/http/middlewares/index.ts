import { RequestHandler } from 'express-serve-static-core';

import { attachmentParser } from './AttachmentsParser';
import { parameterParser } from './ParameterParser';

export const middlewares = (): Array<RequestHandler> => [attachmentParser, parameterParser];

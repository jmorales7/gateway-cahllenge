import { AnyZodObject } from 'zod';

import { GetOrderBookDTO } from '../../../../application/entities/OrderBook/OrderBook.dto';

export type Route = {
  path: string;
  verb: string;
  useCase: any;
  successCode?: number;
  fileBuffer?: boolean;
  schemaValidation?: AnyZodObject | undefined;
};

export const routes: (dependencies: any) => Array<Route> = (dependencies: any) => [
  //OrderBook
  { path: '/order', verb: 'GET', useCase: dependencies.getTips, schemaValidation: GetOrderBookDTO },
  { path: '/market', verb: 'GET', useCase: dependencies.market }
];

import { IOrderBook } from '../../application/entities/OrderBook/IOrderBook';

export type IServices = {
  orderBookService: IOrderBook;
};

import { IDataAccess } from 'infrastructure/data-access/IDataAccess';

import { ILogger } from '../../logger/Logger';
import { IServices } from './IServices';
import { orderBookService } from './orderBook/service';

export const services = (dataAccess: IDataAccess, loggerFactory: ILogger, preInjection: boolean): IServices => {
  const logger = loggerFactory.child({ module: 'services' });
  logger.info('starting services');
  try {
    const orderBookServiceSetUp = orderBookService(dataAccess, logger);
    preInjection ? logger.info('Dependencies pre-injection done') : logger.info('Dependencies fully injected');
    return {
      orderBookService: orderBookServiceSetUp
    };
  } catch (e) {
    logger.error(e, 'failed on start services');
    throw e;
  }
};

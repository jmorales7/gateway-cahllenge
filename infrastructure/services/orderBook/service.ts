import { IOrderBook } from '../../../application/entities/OrderBook/IOrderBook';
import { ILogger } from '../../../logger/Logger';
import { IDataAccess } from '../../data-access/IDataAccess';
import { initialOrderBook } from '../../data-access/bitfinex/Module';

export const orderBookService = ({ bitfinexModule }: IDataAccess, logger: ILogger): IOrderBook => ({
  async getOrderBook({ name }) {
    return await bitfinexModule.getOrderBooks({ name });
  },
  async get({ name }) {
    return initialOrderBook;
  },
  async market(params) {
    return await bitfinexModule.getMarketDep(params);
  }
});

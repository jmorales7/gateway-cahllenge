import { Order, OrderBook } from '../../../application/entities/OrderBook/OrderBook';

export function updateOrderBook(orderBook: OrderBook, price: number, count: number, amount: number) {
  if (count > 0) {
    // Agregar o actualizar el nivel de precio
    if (amount > 0) {
      // Agregar o actualizar una orden de compra (bids)
      updateOrAddOrder(orderBook.bids, price, count, amount);
    } else if (amount < 0) {
      // Agregar o actualizar una orden de venta (asks)
      updateOrAddOrder(orderBook.asks, price, count, amount);
    }
  } else {
    // Eliminar el nivel de precio
    if (amount === 1) {
      // Eliminar una orden de compra (bids)
      removeOrder(orderBook.bids, price);
    } else if (amount === -1) {
      // Eliminar una orden de venta (asks)
      removeOrder(orderBook.asks, price);
    }
  }
}

export function updateOrAddOrder(orders: Order[], price: number, count: number, amount: number) {
  const existingOrder = orders.find((order) => order.price === price);

  if (existingOrder) {
    // Actualizar orden existente
    existingOrder.count = count;
    existingOrder.amount = amount;
  } else {
    // Agregar nueva orden
    orders.push({ price, count, amount });
  }
}

export function removeOrder(orders: Order[], price: number) {
  const index = orders.findIndex((order) => order.price === price);

  if (index !== -1) {
    // Eliminar orden
    orders.splice(index, 1);
  }
}

export function calculateMarketDepth(marketDepthInfo: OrderBook): number {
  let totalBids = 0;
  let totalAsks = 0;

  for (const bid of marketDepthInfo.bids) {
    totalBids += bid.amount;
  }

  for (const ask of marketDepthInfo.asks) {
    totalAsks += ask.amount;
  }

  return totalBids - totalAsks;
}

export function evaluateOrderPrice(
  marketDepthInfo: OrderBook,
  pairName: string,
  operationType: 'buy' | 'sell',
  amount: number
): number {
  const marketDepth = calculateMarketDepth(marketDepthInfo);
  console.log(marketDepth, amount, marketDepth >= amount);
  if (operationType === 'buy') {
    if (marketDepth >= amount) {
      // Sufficient bids to cover the amount
      let totalPrice = 0;
      let remainingAmount = amount;

      for (const bid of marketDepthInfo.bids) {
        if (remainingAmount <= 0) {
          break;
        }

        if (bid.amount >= remainingAmount) {
          // The bid amount is sufficient
          totalPrice += remainingAmount * bid.price;
          remainingAmount = 0;
        } else {
          // Consume the bid and move to the next one
          totalPrice += bid.amount * bid.price;
          remainingAmount -= bid.amount;
        }
      }

      return totalPrice / amount;
    } else {
      // Insufficient bids, return an error value
      return -1;
    }
  } else if (operationType === 'sell') {
    if (-marketDepth <= amount) {
      // Sufficient asks to cover the amount
      let totalPrice = 0;
      let remainingAmount = amount;

      for (const ask of marketDepthInfo.asks) {
        if (remainingAmount >= 0) {
          break;
        }

        if (ask.amount <= remainingAmount) {
          // The ask amount is sufficient
          totalPrice += remainingAmount * ask.price;
          remainingAmount = 0;
        } else {
          // Consume the ask and move to the next one
          totalPrice += ask.amount * ask.price;
          remainingAmount -= ask.amount;
        }
      }

      return totalPrice / amount;
    } else {
      // Insufficient asks, return an error value
      return -1;
    }
  }

  // Invalid operation type, return an error value
  return -1;
}

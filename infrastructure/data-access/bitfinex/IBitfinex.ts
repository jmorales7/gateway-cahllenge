import { GetOrderBookDTO } from '../../../application/entities/OrderBook/OrderBook.dto';

export type IBitfinex = {
  getOrderBooks(params: GetOrderBookDTO): Promise<any>;
  getMarketDep(params: any): Promise<any>;
};

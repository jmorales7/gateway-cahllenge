import ws from 'ws';

import { OrderBook } from '../../../application/entities/OrderBook/OrderBook';
import { CustomError, Errors } from '../../../application/entities/shared/Errors';
import { ILogger } from '../../../logger/Logger';
import { evaluateOrderPrice, updateOrderBook } from './Helpers';
import { IBitfinex } from './IBitfinex';

type BitfinexParams = {
  url: string;
};
export const initialOrderBook: OrderBook = {
  bids: [],
  asks: []
};

export const bitfinexModule = (bitfinex: BitfinexParams, logger: ILogger): IBitfinex => ({
  async getOrderBooks({ name }) {
    const log = logger.child({ function: 'getOrderBooks' });
    try {
      const w = new ws(bitfinex.url);

      w.onmessage = (msg: any) => {
        //console.log(Date.now(), JSON.parse(msg.data))
        if (JSON.parse(msg.data).event !== 'info' && JSON.parse(msg.data).event !== 'subscribed') {
          const [channelId, orders] = JSON.parse(msg.data);

          // Verificar si es un snapshot inicial del order book
          if (Array.isArray(orders[0])) {
            // Limpiar el order book existente
            initialOrderBook.bids = [];
            initialOrderBook.asks = [];

            // Procesar el snapshot inicial y actualizar el order book
            orders.forEach(([price, count, amount]: [number, number, number]) => {
              updateOrderBook(initialOrderBook, price, count, amount);
            });
          } else {
            // Procesar los nuevos datos incrementales y actualizar el order book

            updateOrderBook(initialOrderBook, orders[0], orders[1], orders[2]);
          }
        }
      };
      w.onopen = () => {
        w.send(JSON.stringify({ event: 'subscribe', channel: 'book', symbol: name, prec: 'R0', freq: 'F1', len: 1 }));
      };
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  },
  async getMarketDep({ pairName, operationType, amount }) {
    const log = logger.child({ function: 'getMarketDep' });

    try {
      return evaluateOrderPrice(initialOrderBook, pairName, operationType, amount);
    } catch (e: any) {
      log.error(e);
      if (e instanceof CustomError) {
        throw new CustomError(e.code as Errors, e.message);
      }
      throw new CustomError(Errors.SERVER_ERROR, e);
    }
  }
});

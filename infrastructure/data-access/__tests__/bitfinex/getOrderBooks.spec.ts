import ws from 'ws';

import { OrderBook } from '../../../../application/entities/OrderBook/OrderBook';
import { updateOrderBook } from '../../bitfinex/Helpers';
import { IBitfinex } from '../../bitfinex/IBitfinex';
import { bitfinexModule } from '../../bitfinex/Module';

// Crea un mock para el objeto WebSocket
const mockWebSocketInstance = {
  onmessage: jest.fn(),
  onopen: jest.fn(),
  send: jest.fn()
  // Define otros métodos y propiedades necesarios para el mock
};

// Reemplaza el constructor de ws por el mock
jest.mock('ws', () => jest.fn().mockImplementation(() => mockWebSocketInstance));

// Ahora puedes utilizar el mockWebSocketInstance en tus pruebas

describe('Mi prueba', () => {
  it('debería utilizar el mock del WebSocket', () => {
    // Ejemplo de prueba
    // ...

    // Puedes acceder a los métodos simulados del mock
    mockWebSocketInstance.send('Hola');

    // Puedes verificar si los métodos fueron llamados
    expect(mockWebSocketInstance.send).toHaveBeenCalledWith('Hola');
    expect(mockWebSocketInstance.send).toHaveBeenCalledTimes(1);

    // Puedes verificar cómo se utilizan los métodos simulados en tu código
    // expect(myFunction(mockWebSocketInstance)).toBe(true);
  });
});

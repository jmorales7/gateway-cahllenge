import { IBitfinex } from './bitfinex/IBitfinex';

export type IDataAccess = {
  bitfinexModule: IBitfinex;
};

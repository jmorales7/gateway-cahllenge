import { IConfig } from 'config/Type';

import { ILogger } from '../../logger/Logger';
import { IDataAccess } from './IDataAccess';
import { bitfinexModule } from './bitfinex/Module';

export const dataAccess = async ({ bitfinex }: IConfig, loggerFactory: ILogger): Promise<IDataAccess> => {
  const logger = loggerFactory.child({ module: 'dataAccess' });
  try {
    const bitfinexModuleSetUp = bitfinexModule(bitfinex, logger.child({ dataAccess: 'balanceRepository' }));
    //preInjection ? logger.info('Dependencies pre-injection done') : logger.info('Dependencies fully injected');

    return {
      bitfinexModule: bitfinexModuleSetUp
    };
  } catch (e) {
    logger.error('Failed to initialize Data access with error:', e);
    throw e;
  }
};
